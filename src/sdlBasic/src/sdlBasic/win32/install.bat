rem the installation path
set prefix=%ProgramFiles%\sdlBasic

rem direct install sdlBasic
mkdir %prefix%
mkdir %prefix%\bin
mkdir %prefix%\share\sdlBasic
mkdir %prefix%\share\sdlBasic\plugins

copy ..\..\..\bin\sdlBasic.exe %prefix%\bin
copy ..\..\..\share\sdlBasic\* %prefix%\share\sdlBasic
copy ..\..\..\share\sdlBasic\plugins\* %prefix%\share\sdlBasic\plugins

